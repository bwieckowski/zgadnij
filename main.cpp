#include <iostream>
#include "include/util.h"
#include "include/Quests.h"
#include "include/Player.h"
int main() {

    std::cout << generateQuest<short>() << std::endl;
    std::cout << generateQuest<short>() << std::endl;
    std::cout << (short)generateQuest<char>() << std::endl;
    std::cout << (short)generateQuest<char>() << std::endl;
    std::cout << generateQuest<float>() << std::endl;
    std::cout << generateQuest<float>() << std::endl;
    Quests<int> quests(3);
    quests.newQuestion();
    quests.newQuestion();

    for(int i = 0; i < quests.size(); i++)
        std::cout << quests[i] << std::endl;

    std::cout << "---------------------------------" << std::endl;

    Quests<int>::iterator iter = quests.begin();
    auto iter2(iter);
    for(;iter != quests.end(); iter++, ++iter2)
        std::cout << *iter << std::endl << *iter2 << std::endl;
    Player player;
    Quests<int> questions(2);
    long l = 0;
    std::cout << player.GetID() << std::endl;
    for(auto it = questions.begin(); it != questions.end(); it++){
        player.feedback<int>(2);
        std::cout << "Inside" << std::endl;
        while (true) {

            l += 1;
            std::cout << "Iterator " << l << " *it" << *it << std::endl;
            int answer = player.guess<int>();
            //sleep(1);
            std::cout << "Answer: " << answer << " *it" << *it << std::endl;
            if (answer == *it)
                break;
            else if (answer > *it)
                player.feedback<int>(1);
            else
                player.feedback<int>(-1);
        }
        std::cout << "After" << std::endl;
    }
    std::cout << "After" << std::endl;
    std::cout << "Udało się! " << "Wykonano " << l << " kroków." << std::endl;
    std::cout << player.GetID() << std::endl;
    return 0;
}