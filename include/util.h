//
// Created by Bartek on 07/01/2019.
//

#ifndef ZGADNIJ_UTIL_H
#define ZGADNIJ_UTIL_H

// TODO check that is posiible to get only fundamental type

#include <cstdlib>
#include <random>
#include <chrono>
#include <time.h>
#include <limits>
#include <type_traits>
#include <algorithm>



using namespace std;

template<typename T>
T getMin() {
    return  std::numeric_limits< T >::min();
}

template<typename T>
T getMax() {
    return  std::numeric_limits< T >::max();
}

template < class T >
std::uniform_int_distribution<T> getDistribution(std::true_type, T min, T max)
{
    return std::uniform_int_distribution<T>(min, max);
}

template <class T>
std::uniform_real_distribution<T> getDistribution(std::false_type, T min, T max)
{
    return std::uniform_real_distribution<T>(min, max);
}

template<class T>
T randomRange(T min, T max) {

    std::random_device rd;
    std::default_random_engine e{rd()};
    auto dis = getDistribution<T>(std::integral_constant<bool, std::numeric_limits<T>::is_integer>(), min, max);
    T ret = dis(e);
    return ret;

}

template<class T>
T generateQuest()
{
    T min = getMin<T>();
    T max = getMax<T>();
    return randomRange<T>(min,max);

};

template<class T>
T generateQuest( T min, T max)
{
    return randomRange<T>(min,max);

};

#endif //ZGADNIJ_UTIL_H
