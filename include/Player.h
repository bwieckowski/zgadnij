//
// Created by Bartek on 11/01/2019.
//

#ifndef ZGADNIJ_PLAYER_H
#define ZGADNIJ_PLAYER_H

#include <iostream>
#include "util.h"
#include <memory>

using namespace std;

class Player{

public:
    Player();
    int GetID();

    template< typename T >
    void feedback( int status ){
        this->status = status;
    }


    template< typename T >
    T guess()
    {
        if( status == 2 ) {
            this->actual = generateQuest<T>();
            this->min = getMin<T>();
            this->max = getMax<T>();
        }
        else if( status == -1 )
        {
            this->min = this->actual;
            this->actual = randomRange<T>(min, max);
        } else if( status == 1){
            this->max = this->actual;
            this-> actual = randomRange<T>(min, max);
        }

        return  actual;

    }

private:
    int id;
    int status;
    double long min;
    double long max;
    double long actual;
    static int players_ammount;
};

#endif //ZGADNIJ_PLAYER_H