//
// Created by Bartek on 07/01/2019.
//

#ifndef ZGADNIJ_QUESTS_H
#define ZGADNIJ_QUESTS_H

#include <stdlib.h>
#include "util.h"
#include <memory>
#include <exception>
using namespace std;

template< typename T >
class Quests{

public:

class iterator: public std::iterator<std::forward_iterator_tag, T>{
    public:

        iterator(T* pointer = nullptr): instance(pointer){}
        iterator(const Quests<T>::iterator &it) {
            instance = it.instance;
        }

        bool operator!=(const iterator &it2){ return instance != it2.instance;}

        bool operator==(const iterator &it2){ return instance == it2.instance;}

        iterator operator++(int){
            instance++;
            return *this;
        }

        iterator operator++(){
            iterator i = *this;
            instance++;
            return i;
        }

        T* operator->() {return instance; }
        T& operator*(){return *instance;}

    private:
        T* instance;
};


    Quests(){
        array_size = 0;
        tab = nullptr;
    };

    Quests( int number ){
        //dodac exception size
        array_size = number;
        tab = new T[number];
        for( int i = 0; i < number; i ++)
            tab[i] = generateQuest<T>();
    };




    void newQuestion(){

        T * tmp = new T[array_size];

        for (int i = 0; i < array_size; ++i) {

            tmp[i] = tab[i];
        }

        array_size +=1;
        delete [] tab;
        tab =  new T[array_size];
        for (int i = 0; i < array_size-1; ++i) {
           tab[i] = tmp[i];
        }

        tab[array_size-1] = generateQuest<T>();
        delete [] tmp;
        tmp = 0;

    }

    T & operator []( int id ){
//        if( id >= array_size || id < 0 ){
//
//            std::bad_alloc exception;
//            throw exception;
//        }


        return tab[id];
    }

    int size(){ return array_size; }

    iterator begin() {
        return &tab[0];
    }

    iterator end() {
        return &tab[array_size];
    }



    ~Quests() {
        if(tab != nullptr)
            delete [] tab;
    }

private:
    T * tab;
    int array_size;
};



#endif //ZGADNIJ_QUESTS_H
